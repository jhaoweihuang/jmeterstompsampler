package orgMiJmeterSockjsSampler;

import org.springframework.messaging.simp.stomp.StompSession;

public abstract class JmeterTestService {

    public Integer MESSAGE_COUNTS = 1;

    public Integer INIT_DELAY = 1000;

    public Integer DELAY_TIME = 10000;

    public String SEND_DESTINATION = "/";

    private final StompSession session;

    public JmeterTestService(StompSession session) {
        this.session = session;
    }

    public abstract void test();

    public void send(Object obj) {
        this.session.send(SEND_DESTINATION, obj);
    }

    public String getSessionId() {
        return session.getSessionId();
    }

}
