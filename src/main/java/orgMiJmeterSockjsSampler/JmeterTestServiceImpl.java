package orgMiJmeterSockjsSampler;

import org.springframework.messaging.simp.stomp.StompSession;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class JmeterTestServiceImpl extends JmeterTestService {

    public JmeterTestServiceImpl(StompSession session) {
        super(session);
    }

    static final ScheduledExecutorService SCHEDULER = Executors.newSingleThreadScheduledExecutor(r -> {
        Thread t = new Thread(r);
        t.setDaemon(true);
        return t;
    });

    @Override
    public void test() {
        AtomicInteger runCount = new AtomicInteger(0);
        Runnable runnable = () -> {
            if (runCount.incrementAndGet() > MESSAGE_COUNTS || MESSAGE_COUNTS <= 0) {
                return;
            }
            int counts = runCount.get();
            Message message = new Message();
            message.setSendTime(System.currentTimeMillis());
            message.setName(getSessionId());
            message.setContent(getSessionId() + String.format(": 發送第%s/%s筆測試訊息", counts, MESSAGE_COUNTS));
            send(message);
        };
        SCHEDULER.scheduleAtFixedRate(() -> JmeterTestServiceImpl.SCHEDULER.execute(runnable), INIT_DELAY, DELAY_TIME,
            TimeUnit.MILLISECONDS);
    }

}
