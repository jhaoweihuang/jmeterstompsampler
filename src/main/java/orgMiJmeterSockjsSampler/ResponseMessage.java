package orgMiJmeterSockjsSampler;

import java.util.ArrayList;
import java.util.List;

public class ResponseMessage {

    public Integer MESSAGE_COUNTS = 1;

    public Integer INIT_DELAY = 1000;

    public Integer DELAY_TIME = 10000;

    public String SEND_DESTINATION = "/";

    private String message = "";

    private long messageCounter = 0;

    private String problems = "";

    private final List<Long> costList = new ArrayList<>();

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void addMessage(String message) {
        this.message = (!this.message.equals("")) ? this.message + "\n" + message : message;
    }

    public long getMessageCounter() {
        return messageCounter;
    }

    public void setMessageCounter(long messageCounter) {
        this.messageCounter = messageCounter;
    }

    public String getProblems() {
        return problems;
    }

    public void addProblem(String problem) {
        this.problems = (this.problems != "") ? this.problems + "\n" + problem : problem;
    }

    public void setProblems(String problems) {
        this.problems = problems;
    }

    public void addCostList(long cost) {
        this.costList.add(cost);
    }

    public long getMaxCost() {
        return this.costList.stream().mapToLong(v -> v).max().orElse(-1L);
    }

    public long getMinCost() {
        return this.costList.stream().mapToLong(v -> v).min().orElse(-1L);
    }

    public long getAverageCost() {
        if (this.costList.size() <= 0) {
            return -1L;
        }
        return this.costList.stream().reduce(0L, Long::sum) / this.costList.size();
    }

}
