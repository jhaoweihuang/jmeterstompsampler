package orgMiJmeterSockjsSampler;

import org.springframework.lang.Nullable;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import java.util.Optional;

public class SockJsWebsocketStompSessionHandler extends StompSessionHandlerAdapter {
    private final String subscribeHeaders;

    private final long connectionTime;

    private final long responseBufferTime;

    private final String messageStorage = "";

    private final ResponseMessage responseMessage;

    public SockJsWebsocketStompSessionHandler(String subscribeHeaders, long connectionTime, long responseBufferTime,
        ResponseMessage responseMessage) {
        this.subscribeHeaders = subscribeHeaders;
        this.connectionTime = connectionTime;
        this.responseBufferTime = responseBufferTime;
        this.responseMessage = responseMessage;
    }

    public String getMessageStorage() {
        return this.messageStorage;
    }

    @Override
    public void afterConnected(@Nullable StompSession session, @Nullable StompHeaders connectedHeaders) {
        String connectionMessage =
            "Session id: " + Optional.ofNullable(session).map(StompSession::getSessionId).orElse("[unknown session id]")
                + "\n - Waiting for the server connection for " + this.connectionTime + " MILLISECONDS"
                + "\n - WebSocket connection has been opened" + "\n - Connection established";
        this.responseMessage.addMessage(connectionMessage);
        this.subscribeTo(session);
        JmeterTestService jmeterTestService = new JmeterTestServiceImpl(session);
        jmeterTestService.MESSAGE_COUNTS = this.responseMessage.MESSAGE_COUNTS;
        jmeterTestService.INIT_DELAY = this.responseMessage.INIT_DELAY;
        jmeterTestService.DELAY_TIME = this.responseMessage.DELAY_TIME;
        jmeterTestService.SEND_DESTINATION = this.responseMessage.SEND_DESTINATION;
        jmeterTestService.test();
    }

    @Override
    public void handleException(@Nullable StompSession session, StompCommand command, @Nullable StompHeaders headers,
        @Nullable byte[] payload, Throwable exception) {
        String exceptionMessage = " - Received exception: " + exception.getMessage();
        this.responseMessage.addProblem(exceptionMessage);
    }

    @Override
    public void handleFrame(@Nullable StompHeaders headers, Object payload) {
        String handleFrameMessage =
            " - Received frame: " + Optional.ofNullable(payload).map(Object::toString).orElse("[No Received Payload]");
        this.responseMessage.addMessage(handleFrameMessage);
    }

    @Override
    public void handleTransportError(@Nullable StompSession session, Throwable exception) {
        String exceptionMessage = " - Received exception: " + exception.getMessage();
        this.responseMessage.addProblem(exceptionMessage);
    }

    private void subscribeTo(StompSession session) {
        StompHeaders headers = new StompHeaders();
        String[] splitHeaders = subscribeHeaders.split("\n");
        for (String splitHeader : splitHeaders) {
            int key = 0;
            int value = 1;
            String[] headerParameter = splitHeader.split(":");
            headers.add(headerParameter[key], headerParameter[value]);
        }

        session.subscribe(headers,
            new SockJsWebsocketSubscriptionHandler(this.responseMessage, this.responseBufferTime));
    }
}
