package orgMiJmeterSockjsSampler;

import org.springframework.lang.NonNull;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import java.lang.reflect.Type;
import javax.annotation.Nullable;

public class SockJsWebsocketSubscriptionHandler implements StompFrameHandler {

    private long messageCounter = 1;

    private final long responseBufferTime;

    private final ResponseMessage responseMessage;

    public SockJsWebsocketSubscriptionHandler(ResponseMessage responseMessage, long responseBufferTime) {
        this.responseMessage = responseMessage;
        this.responseBufferTime = responseBufferTime;
        String subscribeMessage = " - Leaving streaming connection open" + "\n - Waiting for messages for "
            + this.responseBufferTime + " MILLISECONDS";
        this.responseMessage.addMessage(subscribeMessage);
    }

    @Override
    @NonNull
    public Type getPayloadType(@Nullable StompHeaders headers) {
        return Message.class;
    }

    @Override
    public void handleFrame(@Nullable StompHeaders headers, Object payload) {
        // try {
        long end = System.currentTimeMillis();
        this.responseMessage.setMessageCounter(this.messageCounter);
        this.messageCounter++;
        Message message = (Message) payload;
        long cost = end - message.getSendTime();
        this.responseMessage.addCostList(cost);
        // this.responseMessage.addMessage(" Received: #" + this.messageCounter + message.toString());
        // } catch (Exception e) {
        // this.responseMessage.addMessage(" Received Error: " + e.getMessage());
        // }
    }
}
